package com.belkevich.generator.metrics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.belkevich.generator.metrics")
public class MetricsGeneratorApplication {

  public static void main(String[] args) {
    SpringApplication.run(MetricsGeneratorApplication.class,args);
  }

}