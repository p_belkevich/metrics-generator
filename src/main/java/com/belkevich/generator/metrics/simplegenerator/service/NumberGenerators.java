package com.belkevich.generator.metrics.simplegenerator.service;

import java.util.Random;
import org.springframework.stereotype.Service;

@Service
public enum NumberGenerators implements Generator {
  INTEGER {
    @Override
    public Integer get() {
      return RandomTemplate.generateInteger();
    }
  },
  DOUBLE {
    @Override
    public Double get() {
      return RandomTemplate.generateDouble();
    }
  };

  private static class RandomTemplate {
    private RandomTemplate() {
    }
    private static final Integer MIN = 0;
    private static final Integer MAX = 10;
    private static Integer generateInteger() {
      return new Random().nextInt((RandomTemplate.MAX - RandomTemplate.MIN) + 1) + RandomTemplate.MIN;
    }
    private static Double generateDouble() {
      return new Random(RandomTemplate.generateInteger()).nextDouble();
    }
  }

}
