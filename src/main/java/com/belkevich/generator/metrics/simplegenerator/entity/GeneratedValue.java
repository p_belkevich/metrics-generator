package com.belkevich.generator.metrics.simplegenerator.entity;

import com.belkevich.generator.metrics.simplegenerator.service.NumberGenerators;

public class GeneratedValue<T extends Number> {
  private T value;

  private void setValue(NumberGenerators numberGenerators) {
    this.value = (T) numberGenerators.get();
  }

  public T generate(NumberGenerators numberGenerators) {
    setValue(numberGenerators);
    return value;
  }
}
