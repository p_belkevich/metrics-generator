package com.belkevich.generator.metrics.simplegenerator.service;

import java.util.function.Supplier;

@FunctionalInterface
public interface Generator<T extends Number> extends Supplier<T> {

}
