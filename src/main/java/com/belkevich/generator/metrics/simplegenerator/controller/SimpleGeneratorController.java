package com.belkevich.generator.metrics.simplegenerator.controller;

import com.belkevich.generator.metrics.simplegenerator.entity.GeneratedValue;
import com.belkevich.generator.metrics.simplegenerator.service.NumberGenerators;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/simple")
public class SimpleGeneratorController {

  @RequestMapping(value = "/integer", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> generateInteger() {
    return new ResponseEntity<>(
        new GeneratedValue<>().generate(NumberGenerators.INTEGER), HttpStatus.OK);
  }

  @RequestMapping(value = "/double", method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> generateDouble() {
    return new ResponseEntity<>(
        new GeneratedValue<>().generate(NumberGenerators.DOUBLE), HttpStatus.OK);
  }
}
